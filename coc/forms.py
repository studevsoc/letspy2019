from django import forms
from .models import Feedback
from django.template.defaultfilters import slugify

class ContactForm(forms.Form):
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = '__all__'
