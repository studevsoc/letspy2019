from django.conf import settings
from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

class coc(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = RichTextField()
    created_date = models.DateTimeField(default=timezone.now)
    coc_date = models.DateTimeField(blank=True, null=True)

    def cocpublish(self):
        self.coc_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Feedback(models.Model):
    name = models.CharField(max_length=200, help_text="victim name")
    email = models.EmailField(max_length=200)
    subject = models.CharField(max_length=200)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Feedback"

    def __str__(self):
        return self.name + "-" +  self.email
