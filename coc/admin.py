from django.contrib import admin
from .models import coc
from .models import Feedback

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'subject','date',)
    search_fields = ('name', 'email',)
    date_hierarchy = 'date'

admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(coc)
