

# Create your views here.
from django.http import HttpResponse
from django.utils import timezone
from .models import coc
from letspy2k19.models import menuBox
from letspy2k19.models import menu
from django.shortcuts import render
from .forms import FeedbackForm
from django.contrib import messages

# Create your views here.

def test_redirect(request):
    c = Category.objects.get(name='python')
    return redirect(c)

def coc_list(request):
    cocs = coc.objects.filter(coc_date__lte=timezone.now()).order_by('coc_date')

    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request,'coc/success.html')
    else:
        form = FeedbackForm()

    return render(request,'coc/coc_list.html',{'cocs': cocs, 'form': form})
