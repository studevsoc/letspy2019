# sds-LETSPY2019


Website for Letspy2k19 by SDS


# Contribution Guide

  * fork the project to your profile
  * clone the project from your profile
  * install virualenv in your local machine using the command

            * `sudo apt-get install python3-dev python3-pip python3-virtualenv`
            * `pip3 install virtualenv`
  * make virtualenv inside the sds-dynamic folder

            * `virtualenv -p python3 env`
            * `.env/bin/activate` //activate the env
  * install django 2.1.3
            * `pip3 install Django==2.1.3`

### done!
