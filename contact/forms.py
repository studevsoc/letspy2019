from django import forms
from .models import contact
from django.template.defaultfilters import slugify

class ContactForm(forms.Form):
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

class contactfrm(forms.ModelForm):

    class Meta:
        model = contact
        fields = '__all__'
