from django.conf import settings
from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField


class contact(models.Model):
    name = models.CharField(max_length=200, help_text="victim name")
    email = models.EmailField(max_length=200)
    subject = models.CharField(max_length=200)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "contact"

    def __str__(self):
        return self.name + "-" +  self.email
