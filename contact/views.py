

# Create your views here.
from django.http import HttpResponse
from django.utils import timezone
from letspy2k19.models import menuBox
from letspy2k19.models import menu
from django.shortcuts import render
from .forms import contactfrm
from django.contrib import messages

# Create your views here.

def test_redirect(request):
    c = Category.objects.get(name='python')
    return redirect(c)

def contact_list(request):

    if request.method == 'POST':
        form = contactfrm(request.POST)
        if form.is_valid():
            form.save()
            return render(request,'contact/success.html')
    else:
        form = contactfrm()

    return render(request,'contact/contact_list.html',{ 'form': form})
