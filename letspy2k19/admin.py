from django.contrib import admin
from .models import menuBox
from .models import faq
from .models import sponsor
from .models import community
from .models import slider
from .models import menu


admin.site.register(menuBox)
admin.site.register(menu)
admin.site.register(faq)
admin.site.register(slider)
admin.site.register(sponsor)
admin.site.register(community)
