

# Create your views here.
from django.http import HttpResponse
from django.utils import timezone
from .models import slider
from .models import menuBox
from .models import menu
from .models import faq
from .models import sponsor
from .models import community
from django.shortcuts import render

# Create your views here.
def index(request):
    menuBoxs = menuBox.objects.filter(menuBox_date__lte=timezone.now()).order_by('menuBox_date')
    sliders = slider.objects.filter(slider_date__lte=timezone.now()).order_by('slider_date')
    faqs=faq.objects.filter(faq_date__lte=timezone.now()).order_by('faq_date')
    sponsors=sponsor.objects.filter(sponsor_date__lte=timezone.now()).order_by('sponsor_date')
    communitys=community.objects.filter(community_date__lte=timezone.now()).order_by('community_date')
    menus=menu.objects.filter(menu_date__lte=timezone.now()).order_by('menu_date')
    return render(request,'letspy2k19/index.html',{'menuBoxs':menuBoxs,'sliders':sliders,'faqs':faqs,'sponsors':sponsors,'communitys':communitys,'menus':menus})
