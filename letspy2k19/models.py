from django.conf import settings
from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

class slider(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    img = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    slider_date = models.DateTimeField(blank=True, null=True)

    def sliderpublish(self):
        self.slider_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class menuBox(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = RichTextField()
    tag = models.CharField(max_length=100,default="home")
    created_date = models.DateTimeField(default=timezone.now)
    menuBox_date = models.DateTimeField(blank=True, null=True)

    def menuBoxpublish(self):
        self.menuBox_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title



class faq(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    question = models.CharField(max_length=200)
    answer = RichTextField()
    created_date = models.DateTimeField(default=timezone.now)
    faq_date = models.DateTimeField(blank=True, null=True)

    def faqpublish(self):
        self.faq_date = timezone.now()
        self.save()

    def __str__(self):
        return self.question

class sponsor(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    sponsorversion = models.CharField(max_length=200)
    sponsorlist = RichTextField()
    created_date = models.DateTimeField(default=timezone.now)
    sponsor_date = models.DateTimeField(blank=True, null=True)

    def sponsorpublish(self):
        self.sponsor_date = timezone.now()
        self.save()

    def __str__(self):
        return self.sponsorversion

class community(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    cname = models.CharField(max_length=200)
    img = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    community_date = models.DateTimeField(blank=True, null=True)

    def communitypublish(self):
        self.community_date = timezone.now()
        self.save()

    def __str__(self):
        return self.cname

class menu(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    menuname = models.CharField(max_length=200)
    tag = models.CharField(max_length=200)
    tagWithHash = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    menu_date = models.DateTimeField(blank=True, null=True)

    def menupublish(self):
        self.menu_date = timezone.now()
        self.save()

    def __str__(self):
        return self.menuname
